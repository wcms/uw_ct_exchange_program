<?php

/**
 * @file
 * uw_ct_exchange_program.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_exchange_program_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_exchange_contact content'.
  $permissions['create uw_exchange_contact content'] = array(
    'name' => 'create uw_exchange_contact content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create uw_exchange_program_block content'.
  $permissions['create uw_exchange_program_block content'] = array(
    'name' => 'create uw_exchange_program_block content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create uw_exchange_student content'.
  $permissions['create uw_exchange_student content'] = array(
    'name' => 'create uw_exchange_student content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create uw_exchange_university content'.
  $permissions['create uw_exchange_university content'] = array(
    'name' => 'create uw_exchange_university content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_exchange_contact content'.
  $permissions['delete any uw_exchange_contact content'] = array(
    'name' => 'delete any uw_exchange_contact content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_exchange_program_block content'.
  $permissions['delete any uw_exchange_program_block content'] = array(
    'name' => 'delete any uw_exchange_program_block content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_exchange_student content'.
  $permissions['delete any uw_exchange_student content'] = array(
    'name' => 'delete any uw_exchange_student content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_exchange_university content'.
  $permissions['delete any uw_exchange_university content'] = array(
    'name' => 'delete any uw_exchange_university content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_exchange_contact content'.
  $permissions['delete own uw_exchange_contact content'] = array(
    'name' => 'delete own uw_exchange_contact content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_exchange_program_block content'.
  $permissions['delete own uw_exchange_program_block content'] = array(
    'name' => 'delete own uw_exchange_program_block content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_exchange_student content'.
  $permissions['delete own uw_exchange_student content'] = array(
    'name' => 'delete own uw_exchange_student content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_exchange_university content'.
  $permissions['delete own uw_exchange_university content'] = array(
    'name' => 'delete own uw_exchange_university content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_exchange_contact content'.
  $permissions['edit any uw_exchange_contact content'] = array(
    'name' => 'edit any uw_exchange_contact content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_exchange_program_block content'.
  $permissions['edit any uw_exchange_program_block content'] = array(
    'name' => 'edit any uw_exchange_program_block content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_exchange_student content'.
  $permissions['edit any uw_exchange_student content'] = array(
    'name' => 'edit any uw_exchange_student content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_exchange_university content'.
  $permissions['edit any uw_exchange_university content'] = array(
    'name' => 'edit any uw_exchange_university content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_exchange_contact content'.
  $permissions['edit own uw_exchange_contact content'] = array(
    'name' => 'edit own uw_exchange_contact content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_exchange_program_block content'.
  $permissions['edit own uw_exchange_program_block content'] = array(
    'name' => 'edit own uw_exchange_program_block content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_exchange_student content'.
  $permissions['edit own uw_exchange_student content'] = array(
    'name' => 'edit own uw_exchange_student content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_exchange_university content'.
  $permissions['edit own uw_exchange_university content'] = array(
    'name' => 'edit own uw_exchange_university content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_exchange_program_block revision log entry'.
  $permissions['enter uw_exchange_program_block revision log entry'] = array(
    'name' => 'enter uw_exchange_program_block revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'enter uw_exchange_student revision log entry'.
  $permissions['enter uw_exchange_student revision log entry'] = array(
    'name' => 'enter uw_exchange_student revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'enter uw_exchange_university revision log entry'.
  $permissions['enter uw_exchange_university revision log entry'] = array(
    'name' => 'enter uw_exchange_university revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_exchange_program_block published option'.
  $permissions['override uw_exchange_program_block published option'] = array(
    'name' => 'override uw_exchange_program_block published option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_exchange_program_block revision option'.
  $permissions['override uw_exchange_program_block revision option'] = array(
    'name' => 'override uw_exchange_program_block revision option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_exchange_student published option'.
  $permissions['override uw_exchange_student published option'] = array(
    'name' => 'override uw_exchange_student published option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_exchange_student revision option'.
  $permissions['override uw_exchange_student revision option'] = array(
    'name' => 'override uw_exchange_student revision option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_exchange_university authored on option'.
  $permissions['override uw_exchange_university authored on option'] = array(
    'name' => 'override uw_exchange_university authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_exchange_university published option'.
  $permissions['override uw_exchange_university published option'] = array(
    'name' => 'override uw_exchange_university published option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
