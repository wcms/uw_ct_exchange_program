<?php

/**
 * @file
 * uw_ct_exchange_program.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_exchange_program_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "taxonomy_display" && $api == "taxonomy_display") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_exchange_program_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function uw_ct_exchange_program_image_default_styles() {
  $styles = array();

  // Exported image style: flag_thumb.
  $styles['flag_thumb'] = array(
    'name' => 'flag_thumb',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 30,
          'height' => 20,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'flag_thumb',
  );

  // Exported image style: student_profile_image.
  $styles['student_profile_image'] = array(
    'name' => 'student_profile_image',
    'effects' => array(
      2 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 340,
          'height' => 200,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'student_profile_image',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_ct_exchange_program_node_info() {
  $items = array(
    'uw_exchange_contact' => array(
      'name' => t('Exchange Contact'),
      'base' => 'node_content',
      'description' => t('A contact information for outgoing exchange students'),
      'has_title' => '1',
      'title_label' => t('Contact'),
      'help' => '',
    ),
    'uw_exchange_program_block' => array(
      'name' => t('Exchange Program Block'),
      'base' => 'node_content',
      'description' => t('A editable block for exchange program home page'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'uw_exchange_student' => array(
      'name' => t('Exchange Profile'),
      'base' => 'node_content',
      'description' => t('A student\'s profile for outgoing exchange students'),
      'has_title' => '1',
      'title_label' => t('Profile title'),
      'help' => '',
    ),
    'uw_exchange_university' => array(
      'name' => t('Exchange University'),
      'base' => 'node_content',
      'description' => t('A university for outgoing exchange students'),
      'has_title' => '1',
      'title_label' => t('Exchange university'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
