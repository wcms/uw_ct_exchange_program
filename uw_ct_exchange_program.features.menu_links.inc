<?php
/**
 * @file
 * uw_ct_exchange_program.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_exchange_program_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_exchange-program-data-report:admin/exchange-program-data-report.
  $menu_links['menu-site-management_exchange-program-data-report:admin/exchange-program-data-report'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/exchange-program-data-report',
    'router_path' => 'admin/exchange-program-data-report',
    'link_title' => 'Exchange program data report',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_exchange-program-data-report:admin/exchange-program-data-report',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies_country:admin/structure/taxonomy/uwaterloo_country.
  $menu_links['menu-site-manager-vocabularies_country:admin/structure/taxonomy/uwaterloo_country'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uwaterloo_country',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Country',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_country:admin/structure/taxonomy/uwaterloo_country',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies_faculty:admin/structure/taxonomy/uwaterloo_faculty.
  $menu_links['menu-site-manager-vocabularies_faculty:admin/structure/taxonomy/uwaterloo_faculty'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uwaterloo_faculty',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Faculty',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_faculty:admin/structure/taxonomy/uwaterloo_faculty',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies_program-type:admin/structure/taxonomy/uwaterloo_program_type.
  $menu_links['menu-site-manager-vocabularies_program-type:admin/structure/taxonomy/uwaterloo_program_type'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uwaterloo_program_type',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Program type',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_program-type:admin/structure/taxonomy/uwaterloo_program_type',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Country');
  t('Exchange program data report');
  t('Faculty');
  t('Program type');

  return $menu_links;
}
