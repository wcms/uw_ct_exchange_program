<?php

/**
 * @file
 * uw_ct_exchange_program.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_exchange_program_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'exchange_profile_under_all_faculties';
  $context->description = 'Display exchange student profile under all faculty page';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'outgoing-exchange-students/exchange-programs/all-faculties' => 'outgoing-exchange-students/exchange-programs/all-faculties',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-7e0cee13cf3d450d155cdd38e1600549' => array(
          'module' => 'views',
          'delta' => '7e0cee13cf3d450d155cdd38e1600549',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display exchange student profile under all faculty page');
  $export['exchange_profile_under_all_faculties'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'exchange_profile_university_page';
  $context->description = 'Display student profiles under university page';
  $context->tag = 'Content';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'uw_exchange_university' => 'uw_exchange_university',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'node/*' => 'node/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-c523ef084776835cb22b2d7e3ec691db' => array(
          'module' => 'views',
          'delta' => 'c523ef084776835cb22b2d7e3ec691db',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display student profiles under university page');
  $export['exchange_profile_university_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'exchange_student_profile_sidebar_content';
  $context->description = 'Displays exchange student profile sidebar content.';
  $context->tag = 'Content';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'uw_exchange_student' => 'uw_exchange_student',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_exchange_program-uw_exchange_sidebar_block' => array(
          'module' => 'uw_ct_exchange_program',
          'delta' => 'uw_exchange_sidebar_block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'uw_ct_exchange_program-uw_exchange_sidebar_links' => array(
          'module' => 'uw_ct_exchange_program',
          'delta' => 'uw_exchange_sidebar_links',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays exchange student profile sidebar content.');
  $export['exchange_student_profile_sidebar_content'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'exchange_university_home_page';
  $context->description = 'Display exchange universities by faculty and country';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'outgoing-exchange-students/exchange-programs' => 'outgoing-exchange-students/exchange-programs',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'service_links-service_links' => array(
          'module' => 'service_links',
          'delta' => 'service_links',
          'region' => 'content',
          'weight' => '39',
        ),
        'views-d00ee36aa491d907bcd15cb7dc56e5b5' => array(
          'module' => 'views',
          'delta' => 'd00ee36aa491d907bcd15cb7dc56e5b5',
          'region' => 'content',
          'weight' => '30',
        ),
        'nodeblock-48' => array(
          'module' => 'nodeblock',
          'delta' => '48',
          'region' => 'content',
          'weight' => '34',
        ),
        'views-c7849856aa3b0ea17bb3d428a4511cad' => array(
          'module' => 'views',
          'delta' => 'c7849856aa3b0ea17bb3d428a4511cad',
          'region' => 'content',
          'weight' => '35',
        ),
        'nodeblock-49' => array(
          'module' => 'nodeblock',
          'delta' => '49',
          'region' => 'content',
          'weight' => '36',
        ),
        'views-b27ea45be8bde615097343da94008a03' => array(
          'module' => 'views',
          'delta' => 'b27ea45be8bde615097343da94008a03',
          'region' => 'content',
          'weight' => '37',
        ),
        'views-7e0cee13cf3d450d155cdd38e1600549' => array(
          'module' => 'views',
          'delta' => '7e0cee13cf3d450d155cdd38e1600549',
          'region' => 'content',
          'weight' => '38',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display exchange universities by faculty and country');
  $export['exchange_university_home_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'exchange_university_profile_under_country';
  $context->description = 'Display exchange university and student profile under country term page';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'country/*' => 'country/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-06a387a28a1c512cdb0b3132e665a873' => array(
          'module' => 'views',
          'delta' => '06a387a28a1c512cdb0b3132e665a873',
          'region' => 'content',
          'weight' => '35',
        ),
        'views-f7578fed8795cd4254691ba6915a57bb' => array(
          'module' => 'views',
          'delta' => 'f7578fed8795cd4254691ba6915a57bb',
          'region' => 'content',
          'weight' => '40',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display exchange university and student profile under country term page');
  $export['exchange_university_profile_under_country'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'exchange_university_profile_under_faculty';
  $context->description = 'Display exchange university and student profile under faculty term page';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'faculty/*' => 'faculty/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-591260b53c2ca9904d1696149d896875' => array(
          'module' => 'views',
          'delta' => '591260b53c2ca9904d1696149d896875',
          'region' => 'content',
          'weight' => '35',
        ),
        'views-9b763900dd55783558688f6a6dcee10b' => array(
          'module' => 'views',
          'delta' => '9b763900dd55783558688f6a6dcee10b',
          'region' => 'content',
          'weight' => '40',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display exchange university and student profile under faculty term page');
  $export['exchange_university_profile_under_faculty'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'exchange_university_profile_under_program';
  $context->description = 'Display exchange university and student profile under faculty term page';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'program-type/*' => 'program-type/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-7e7ddd7c7f43205509184608c763291b' => array(
          'module' => 'views',
          'delta' => '7e7ddd7c7f43205509184608c763291b',
          'region' => 'content',
          'weight' => '40',
        ),
        'views-64acc0c353d9c69ee7cdcf5b68fdca57' => array(
          'module' => 'views',
          'delta' => '64acc0c353d9c69ee7cdcf5b68fdca57',
          'region' => 'content',
          'weight' => '35',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display exchange university and student profile under faculty term page');
  $export['exchange_university_profile_under_program'] = $context;

  return $export;
}
